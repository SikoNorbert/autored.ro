create database autored;

use autored;

create table menus
( menuID int auto_increment not null primary key,
  menuSection varchar(255) not null,
  AutoredWebsite varchar(255) not null,
  listOfProducts varchar(255),
  products varchar(255),
  price int(255),
  productCorrectness boolean not null	
);

create table autenthification_menu
( authentificationID int auto_increment not null primary key,
  registration varchar(255) not null,
  customerRegistration varchar(255) not null,
  typeOfPerson varchar(255),
  myName varchar(255),
  email varchar(255),
  myPassword varchar(255),
  shire varchar(255),
  city varchar(255),
  street varchar(255),
  number int(255),
  mobilphone varchar(255),
  checkboxTermAndCond varchar(255),
  checkboxDataProccessing varchar(255),
  creationOfMyAccount varchar(255)
);

create table bug_authentification_table
( bugID int auto_increment not null primary key,
  registration varchar(255) not null,
  customerRegistration varchar(255) not null,
  typeOfPerson varchar(255),
  myName varchar(255),
  email varchar(255),
  myPassword varchar(255),
  CUI varchar(255),
  companyName varchar(255),
  shire varchar(255),
  city varchar(255),
  addressOfSocialSediu varchar(255),
  regComerOrder varchar(255),
  delegate varchar(255),
  IBAN varchar(255),
  bank varchar(255),
  telephone varchar(255),
  checkboxTermAndCond varchar(255),
  checkboxDataProccessing varchar(255),
  creationOfMyAccount varchar(255)
);

create table consultancy_menu
( consultancyID int auto_increment not null primary key,
  consultancyName varchar(255) not null,
  consultancyEmail varchar(255) not null,
  consultancyTelephone varchar(255) not null,
  carBodySeriesVIN varchar(255) not null,
  enumerationPieces varchar(255) not null
);

create table pieces
( carPieces varchar(255)
);

alter table pieces
add carID int auto_increment not null primary key;

create table carMarks
( ID int auto_increment not null primary key,
  carMark varchar(255) not null,	
  factoryYear float8 not null,	
  model varchar(255) not null,
  fuel varchar(255) not null,
  modelWithCharacteristics varchar(255) not null,
  engine varchar(255) not null,
  components varchar(255) not null
);

alter table carMarks
add manufacturer varchar(255) not null;

alter table carMarks
add codProduct varchar(255) not null;

alter table carMarks
add price float not null;