package stepImpl;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import autoredUtils.MenuUtils;
import org.junit.Assert;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MenuSection {

   model.MenuSection menu = new model.MenuSection("PIESE UNIVERSALE", false);

    @Given("^this (.*) on the (.*)$")
    public void thisMenuSectionOnTheAutoredWebsite(String menuSection, String AutoredWebsite) {
        MenuUtils.addMenuSectionForTheAutoredWebsite();
        menu.setMenuSection(menuSection);
        for(int i = menu.getMenuSection().indexOf(0); i <= menu.getMenuSection().indexOf(6); i++) {
            Assert.assertEquals(MenuUtils.addMenuSectionForTheAutoredWebsite(), menu.addCopyOfMenuSectionAndWebsitePages());
        }
    }

    @When("^I choose the (.*)$")
    public void iChooseTheListOfProducts(String listOfProducts) {
        System.out.println(MenuUtils.chooseTheProducts());
    }

    @And("^sort the (.*) in ascendent form by (.*)$")
    public void sortTheProductsInAscendentFormByPrice(String products, int price) {
        menu.addProductsToTheList();
        menu.addPriceToTheList();
    }

    @Then("^check the (.*) in the database$")
    public void checkTheProductCorrectnessInTheDatabase(boolean productCorrectness) {
        menu.setProductCorrectness(productCorrectness);
        if(menu.isProductCorrectness() == true || menu.isProductCorrectness() == false){
        try {
            MenuUtils.addProductReservation();
        } catch (StackOverflowError e) {
            System.out.println(e.getSuppressed());
        }
        Assert.assertEquals(menu.isProductCorrectness(), productCorrectness);
        } else {
            if(menu.isProductCorrectness() != true || menu.isProductCorrectness() != false) {
                Assert.assertEquals("tru", productCorrectness);
                Assert.assertEquals("fals", productCorrectness);
            }
        }
    }
}
