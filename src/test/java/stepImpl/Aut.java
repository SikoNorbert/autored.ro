package stepImpl;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import model.AuthentificationModel;
import org.junit.Assert;
import autoredUtils.AuthentificationUtils;

public class Aut {
    AuthentificationModel auth = new AuthentificationModel("Autored database registration page");
    @Given("^I reach the (.*) page$")
    public void iGetTheRegistration(String registration) {
        auth.setRegistration(registration);
        for(int i = 0; i <= 3; i++)
        {
            System.out.println(auth.getRegistration());
        }
    }

    @When("^I type (.*), (.*), (.*), (.*), (.*)$")
    public void iTypeCustomerRegistrationTypeOfPersonNameEmailPassword(String customerRegistration, String typeOfPerson, String name, String email, String password) {
        auth.setCustomerRegistration(customerRegistration);
        auth.setTypeOfPerson(typeOfPerson);
        auth.setName(name);
        auth.setEmail(email);
        auth.setPassword(password);
        Assert.assertEquals(auth.getCustomerRegistration(), customerRegistration);
        Assert.assertEquals(auth.getTypeOfPerson(), typeOfPerson);
        Assert.assertEquals(auth.getName(), name);
        Assert.assertEquals(auth.getEmail(), email);
        Assert.assertEquals(auth.getPassword(), password);

        System.out.println(AuthentificationUtils.addAllObjects());

    }

    @And("^addres (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*) and my telephone (.*)$")
    public void addresShireCityStreetNumberBlockScaleApartmentFloorAndMyTelephoneMobilPhone(String shire, String city, String street, float number, int block, int scale, int apartment, int floor, String mobilphone) {
        auth.setShire(shire);
        auth.setCity(city);
        auth.setStreet(street);
        auth.setNumber(number);
        auth.setBlock(block);
        auth.setScale(scale);
        auth.setFloor(floor);
        auth.setMobilphone(mobilphone);
        Assert.assertEquals(auth.getShire(), shire);
        Assert.assertEquals(auth.getCity(), city);
        Assert.assertEquals(auth.getStreet(), street);
        Assert.assertEquals(auth.getNumber(), number, 1 * Math.pow(10, -26));
        Assert.assertEquals(auth.getBlock(), block);
        Assert.assertEquals(auth.getScale(), scale);
        Assert.assertEquals(auth.getFloor(), floor);
        Assert.assertEquals(auth.getMobilphone(), mobilphone);

        System.out.println(AuthentificationUtils.addAllObjects());
    }

    @And("^I accept all the term and conditions (.*) and the details acceptances (.*)$")
    public void iAcceptAllTheTermAndConditionsCheckboxTermAndCondAndTheDetailsAcceptancesCheckboxDataProccessing(String checkboxTermAndCond, String checkboxDataProccessing) {
        auth.setCheckboxTermAndCond(checkboxTermAndCond);
        auth.setCheckboxDataProccessing(checkboxDataProccessing);
        Assert.assertEquals(auth.getCheckboxTermAndCond(), checkboxTermAndCond);
        Assert.assertEquals(auth.getCheckboxDataProccessing(), checkboxDataProccessing);

        System.out.println(auth.getCheckboxTermAndCond());
        System.out.println(auth.getCheckboxDataProccessing());

    }

    @Then("^my account (.*) is created$")
    public void myAccountCreationOfMyAccountIsActivated(String creationOfMyAccount) {
        auth.setCreationOfMyAccount(creationOfMyAccount);
        Assert.assertEquals(auth.getCreationOfMyAccount(), creationOfMyAccount);

        System.out.println(auth.getCreationOfMyAccount());
    }

    @Given("^website (.*)$")
    public void websiteRegistration(String registration) {
        auth.setRegistration(registration);
        Assert.assertEquals(auth.getRegistration(), registration);

        System.out.println(auth.getRegistration());
    }

    @When("^I type (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)$")
    public void iTypeCustomerRegistrationTypeOfPersonNameEmailPasswordCUICompanyNameShireCityAddressOfSocialSediuRegComerOrderDelegateIBANBankTelephone(String customerRegistration, String typeOfPerson, String name, String email, String password, String CUI, String companyName, String shire, String city, String addressOfSocialSediu, String regComerOrder, String delegate, String IBAN, String bank, String telephone) {
        auth.setCustomerRegistration(customerRegistration);
        auth.setTypeOfPerson(typeOfPerson);
        auth.setName(name);
        auth.setEmail(email);
        auth.setPassword(password);
        auth.setCUI(CUI);
        auth.setCompanyName(companyName);
        auth.setShire(shire);
        auth.setCity(city);
        auth.setAddressOfSocialSediu(addressOfSocialSediu);
        auth.setRegComerOrder(regComerOrder);
        auth.setDelegate(delegate);
        auth.setIBAN(IBAN);
        auth.setBank(bank);
        auth.setTelephone(telephone);

            Assert.assertEquals(auth.getCustomerRegistration(), customerRegistration);
            Assert.assertEquals(auth.getTypeOfPerson(), typeOfPerson);
            Assert.assertEquals(auth.getName(), name);
            Assert.assertEquals(auth.getEmail(), email);
            Assert.assertEquals(auth.getPassword(), password);
            Assert.assertEquals(auth.getCUI(), CUI);
            Assert.assertEquals(auth.getCompanyName(), companyName);
            Assert.assertEquals(auth.getShire(), shire);
            Assert.assertEquals(auth.getCity(), city);
            Assert.assertEquals(auth.getAddressOfSocialSediu(), addressOfSocialSediu);
            Assert.assertEquals(auth.getRegComerOrder(), regComerOrder);
            Assert.assertEquals(auth.getDelegate(), delegate);
            Assert.assertEquals(auth.getIBAN(), IBAN);
            Assert.assertEquals(auth.getBank(), bank);
            Assert.assertEquals(auth.getTelephone(), telephone);

            System.out.println(auth.getCustomerRegistration());
            System.out.println(auth.getTypeOfPerson());
            System.out.println(auth.getName());
            System.out.println(auth.getEmail());
            System.out.println(auth.getPassword());
            System.out.println(auth.getCUI());
            System.out.println(auth.getCompanyName());
            System.out.println(auth.getShire());
            System.out.println(auth.getCity());
            System.out.println(auth.getAddressOfSocialSediu());
            System.out.println(auth.getRegComerOrder());
            System.out.println(auth.getDelegate());
            System.out.println(auth.getIBAN());
            System.out.println(auth.getBank());
            System.out.println(auth.getTelephone());

            System.out.println((AuthentificationUtils.addJuridicPersonToTheAccountRegistration()));

    }
    @And("^I accept the term and conditions (.*) and the details acceptances (.*)$")
    public void iAcceptTheTermAndConditionsCheckboxTermAndCondAndTheDetailsAcceptancesCheckboxDataProcessing(String checkboxTermAndCond, String checkboxDataProccessing) {
        auth.setCheckboxTermAndCond(checkboxTermAndCond);
        auth.setCheckboxDataProccessing(checkboxDataProccessing);
        Assert.assertEquals(auth.getCheckboxTermAndCond(), checkboxTermAndCond);
        Assert.assertEquals(auth.getCheckboxDataProccessing(), checkboxDataProccessing);

        System.out.println(auth.getCheckboxTermAndCond());
        System.out.println(auth.getCheckboxDataProccessing());
    }

    @Then("^my account (.*) is accepted$")
    public void myAccountCreationOfMyAccountIsActivating(String creationOfMyAccount) {
        auth.setCreationOfMyAccount(creationOfMyAccount);
        Assert.assertEquals(auth.getCreationOfMyAccount(), creationOfMyAccount);

        System.out.println(auth.getCreationOfMyAccount());
    }
}