package stepImpl;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import model.Autored;
import org.junit.Assert;
import autoredUtils.AutoredUtils;

public class FirstSection {
    Autored autored = new Autored();
    @Given("^the (.*)$")
    public void theWebsite(String website) {
        autored.setWebsite(website);
        String actualWebsite = autored.getWebsite();
        System.out.println(actualWebsite);
        Assert.assertEquals(actualWebsite, website);
    }

    @When("^I add (.*), (.*) and (.*)$")
    public void iAddTitleLogoAndJumbotron(String title, String logo, String jumbotron) {
        autored.setTitle(title);
        String actualTitle = autored.getTitle();
        System.out.println(actualTitle);
        Assert.assertEquals(actualTitle, title);

        autored.setLogo(logo);
        String actualLogo = autored.getLogo();
        System.out.println(actualLogo);
        Assert.assertEquals(actualLogo, logo);

        autored.setJumbotron(jumbotron);
        String actualJumbotron = autored.getJumbotron();
        System.out.println(actualJumbotron);
        Assert.assertEquals(actualJumbotron, jumbotron);
    }


    @Then("^I list a group of searchBar to show with (.*) that exist$")
    public void iListAGroupOfSearchBarToShowWithAutoredIdThatExist(int autoredId) {
        autored.setAutoredId(autoredId);
        int autoredID = autored.getAutoredId();
        Assert.assertEquals(autoredID, autoredId);
        System.out.println(AutoredUtils.addToSearchBar() + "\n");
    }
}
