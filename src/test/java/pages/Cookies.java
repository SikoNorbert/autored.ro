package pages;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

@Getter
@Setter
public class Cookies {

    WebDriver driver;
    private String cookieDescription = "Folosim cookie-uri pentru a-ti oferi cea mai buna experienta pe site-ul nostru. " +
            "Poti afla mai multe despre ce cookie-uri folosim sau sa le dezactivezi in setari";

    public Cookies(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
