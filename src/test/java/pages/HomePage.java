package pages;

import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
@Setter
public class HomePage {
    private String title = "Autored - Catalog online de piese auto autored.ro";
    private String baseURL = "https://www.autored.ro/";
    WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id=\"header\"]/div[1]/div/div[1]/h1/a/img")
    private WebElement logo;

    @FindBy(name = "s")
    private WebElement searchbar;

    @FindBy(xpath = "//*[@id=\"header\"]/div[1]/div/div[2]/div/form/div[1]/span[2]/button")
    private WebElement searchMagnifier;

    @FindBy(className = "cart-head")
    private WebElement shoppingCart;

    @FindBy(className = "contul-meu")
    private WebElement myAccount;

    @FindBy(xpath = "//*[@id=\"content\"]/article/div/div[2]/div[1]/div/div/div/div")
    private WebElement productIcon;

    // solicita pret button
    @FindBy(xpath = "//*[@id=\"content\"]/article/div/div[1]/div/div/div/div/div[2]/div[2]/div/div[1]/div/div/a")
    private WebElement orderButton;

    @FindBy(className = "menu-title")
    private WebElement pieseUniversale;

    @FindBy(id = "nav-menu-item-10")
    private WebElement unelteSiScule;

    @FindBy(xpath = "//*[@id=\"nav-menu-item-10\"]/div")
    private WebElement dropdownMenu;

    @FindBy(id = "nav-menu-item-28")
    private WebElement cricuri;

    @FindBy(id = "nav-menu-item-54")
    private WebElement uleiDeMotor;

    @FindBy(xpath = "//*[@id=\"content\"]/article/div/div[2]/div[1]/div/div/div/div/a/img")
    private WebElement clickOnUleiDeMotor;

    @FindBy(xpath = "//*[@id=\"adaugaCosWebis286\"]")
    private WebElement adaugaInCos;

    @FindBy(xpath = "//*[@id=\"content\"]/article/div/div[1]/div/div[1]/div/div/div[2]/div[2]/div/div[1]/div/div/a")
    private WebElement comandaRapid;

    public boolean checkThatIamOnHomePage() throws NullPointerException {
        if (getLogo().isDisplayed() && getSearchbar().isDisplayed() && getShoppingCart().isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }

    public void checkTheInputInTheSearchbarAndTheValues() {
        try {
            getSearchbar().sendKeys("BMW");
            getSearchMagnifier().click();
            getProductIcon().click();
            getOrderButton().click();


        } catch (ElementNotInteractableException c) {
            System.out.println(c.getMessage());
        }
    }

    public void checkTheMenuTitleInputProductsOnBasket() {
        int option = 1;
        getPieseUniversale().click();
        getUleiDeMotor().click();
        getClickOnUleiDeMotor().click();
        if (getDropdownMenu().isSelected() && getCricuri().isDisplayed()) {
            getDropdownMenu().click();
            getCricuri().click();
        }

        switch (option) {
            case 1: {
                getAdaugaInCos().click();
                break;
            }
            default: {
                getComandaRapid().click();
                break;
            }
        }
        checkTheInputInTheSearchbarAndTheValues();
    }

}


