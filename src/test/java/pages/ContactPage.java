package pages;

import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.LinkedList;


@Getter
@Setter
public class ContactPage {
    private String baseURL = "https://www.autored.ro/contact";
    private String whatsAppApiURL = "https://api.whatsapp.com/send/?phone=40786113308&text&app_absent=0";
    private String contactPageTitle = "DATE DE CONTACT";
    private String contactInformationsAboutTheDistributionCompany;
    WebDriver driver;

    public ContactPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id=\"content\"]/article/div/div[1]/div/div/div[2]/div[2]/div/div/div/p[1]/a")
    private WebElement mobilPhone;

    @FindBy(xpath = "//*[@id=\"content\"]/article/div/div[1]/div/div/div[2]/div[2]/div/div/div/p[2]/a[1]")
    private WebElement email1;

    @FindBy(xpath = "//*[@id=\"content\"]/article/div/div[1]/div/div/div[2]/div[2]/div/div/div/p[2]/a[2]")
    private WebElement email2;

    @FindBy(xpath = "//*[@id=\"content\"]/article/div/div[1]/div/div/div[2]/div[2]/div/div/div/p[3]/a")
    private WebElement whatsApp;

    @FindBy(xpath = "//*[@id=\"wpcf7-f331-p302-o1\"]/form/div[1]/span/input")
    private WebElement name;

    @FindBy(xpath = "//*[@id=\"wpcf7-f331-p302-o1\"]/form/div[2]/span/input")
    private WebElement email;

    @FindBy(xpath = "//*[@id=\"wpcf7-f331-p302-o1\"]/form/div[3]/span/input")
    private WebElement telephone;

    @FindBy(xpath = "//*[@id=\"wpcf7-f331-p302-o1\"]/form/div[4]/span/textarea")
    private WebElement textArea;

    @FindBy(xpath = "//*[@id=\"wpcf7-f331-p302-o1\"]/form/div[5]/input")
    private WebElement submit;

    public boolean setUpTheContactForm() {
        LinkedList<ContactPage> contact = new LinkedList<>();
        ContactPage contactPage = new ContactPage(driver);
        contactPage.setContactInformationsAboutTheDistributionCompany("AUTORED.RO S.R.L.");
        contactPage.setContactInformationsAboutTheDistributionCompany("Cod fiscal: 41702371");
        contactPage.setContactInformationsAboutTheDistributionCompany("Nr. Reg. Com: J22/3084/2019");
        contactPage.setContactInformationsAboutTheDistributionCompany("Adresa: Bulevardul Poitiers nr. 16, 700671, Iasi, Romania");
        contactPage.setContactInformationsAboutTheDistributionCompany("Cont Bancar: RO04BTRLRONCRT0522269701\n" +
                "Banca Transilvania");
        contact.add(contactPage);

        return true;
    }

    public void inputMyBeneficiaryContactInformationForTheContactForm() {
        getContactPageTitle();
        System.out.println("I check the functionalities below : ");
        System.out.println("Suport Clienti");
        int option = 1;
            System.out.println("Telefon: ");

            switch (option) {
                case 1: {
                    if(option == 1){
                    WebDriverWait wait = new WebDriverWait(driver,8);
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"content\"]/article/div/div[1]/div/div/div[2]/div[2]/div/div/div/p[1]/a")));
                    getMobilPhone().click();
                    option++;
                    break;
                   }
                }
                case 2: {
                    if (option == 2) {
                        WebDriverWait wait2 = new WebDriverWait(driver,8);
                        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"content\"]/article/div/div[1]/div/div/div[2]/div[2]/div/div/div/p[2]/a[1]")));
                        getEmail1().click();
                        option++;
                        break;
                    }
                }
                case 3: {
                    if (option == 3) {
                        WebDriverWait wait3 = new WebDriverWait(driver,8);
                        wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"content\"]/article/div/div[1]/div/div/div[2]/div[2]/div/div/div/p[2]/a[2]")));
                        getEmail2().click();
                        option++;
                        break;
                    }
                }
                default: {
                    WebDriverWait wait4 = new WebDriverWait(driver,8);
                    wait4.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"content\"]/article/div/div[1]/div/div/div[2]/div[2]/div/div/div/p[3]/a")));
                    getWhatsApp().click();
                    break;
            }
        }
        // The script that will will open a new blank window
        // If you want to open a link new tab, replace 'about:blank' with a link
        String a = "window.open('about:blank','_blank');";
        ((JavascriptExecutor) driver).executeScript(a);
        driver.navigate().to(whatsAppApiURL);
    }
    public void sendInformationsToContactForm(){
        getName().sendKeys("Siko Norbert");
        getEmail().sendKeys("norbiateniszes@yahoo.com");
        getTelephone().sendKeys("0742.190.240");
        getTextArea().sendKeys("Am rugamintea sa-mi trimiteti informatii despre masini si piese");
        try {
            getSubmit().click();
        } catch(ElementClickInterceptedException e) {
            System.out.println(e.getSuppressed());
        }
    }
}
