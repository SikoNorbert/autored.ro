package pages;

import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
@Setter
public class AutoMarkAttach {

    WebDriver driver;

    public AutoMarkAttach(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id=\"content\"]/article/div/div[3]/div/div/div/div/div/div[2]/div[3]")
    private WebElement BMW;

    @FindBy(xpath = "//*[@id=\"content\"]/article/div/div[3]/div/div/div/div/div/div[2]/div[3]/div/div[2]/div[1]/div[3]")
    private WebElement BMWFactoryYear;

    @FindBy(xpath = "//*[@id=\"content\"]/article/div/div[1]/div/div[3]/div[32]/div")
    private WebElement automark;

    public void setTheAutoMark() {
        getBMW().click();
        getBMWFactoryYear().getLocation();
        try {
            getBMWFactoryYear().click();
        } catch (ElementNotInteractableException e) {
            System.out.println(e.getMessage());
        }
    }
}
