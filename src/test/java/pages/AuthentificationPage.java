package pages;

import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

@Getter
@Setter
public class AuthentificationPage {
    private String baseURL = "https://www.autored.ro/autentificare/";
    WebDriver driver;

    public AuthentificationPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

}
