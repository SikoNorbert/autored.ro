package pages;

public enum Pieces {
    OGLINDA, AMORTIZOR, FARURI, STOPURI, LAMPA, CARENAJ, PROIECTOARE, MANER,
    RADIATOR, USI, CARCASA, BANDOURI, ARIPA, CAPOTA, CHEDERE,
    CARLIG, MODUL, SPOILER, MOTOR, LONJERON, TRAGER, ARMATURA, EMBLEMA;

    Pieces() {
        System.out.println("Constructor called for : " + this.toString());
    }

    public static void main(String[] args) {
        for(Pieces p : Pieces.values())
        System.out.println(p);
    }
}

