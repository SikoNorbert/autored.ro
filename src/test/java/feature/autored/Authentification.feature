Feature: I entry to the website registration page

  Scenario Outline: I am testing the authentification
    Given I reach the <registration> page
    When I type <customerRegistration>, <typeOfPerson>, <name>, <email>, <password>
    And addres <shire>, <city>, <street>, <number>, <block>, <scale>, <apartment>, <floor> and my telephone <mobilphone>
    And I accept all the term and conditions <checkboxTermAndCond> and the details acceptances <checkboxDataProccessing>
    Then my account <creationOfMyAccount> is created

    Examples:
      | registration                        | customerRegistration | typeOfPerson | name             | email                    | password  | shire      | city          | street    | number | block | scale | apartment | floor | mobilphone     | checkboxTermAndCond | checkboxDataProccessing | creationOfMyAccount |
      | https://www.autored.ro/inregistrare | passed               | Physic       | Siko Norbert     | norbiateniszes@yahoo.com | 123456    | MURES      | REGHIN        | Baii      | 22     | 0     | 0     | 0         | 0     | 0742.190.240   | checked             | checked                 | passed              |
      | https://www.autored.ro/inregistrare | failed               | Physic       | Alex Fierascu    | ioan.alexf@gmail.com     | abcdef    | IASI       | IASI          | Bogatilor | 9      | 0     | 0     | 0         | 0     | 0755.321.999   | not checked         | not checked             | failed              |
      | https://www.autored.ro/inregistrare | passed               | Physic       | Siko J Norbert   | sikonorbert0@gmail.com   | prostul   | MURES      | MURES         | Iernuteni | 175    | 0     | 0     | 0         | 0     | 0742.190.240   | checked             | checked                 | passed              |
      | https://www.autored.ro/inregistrare | failed               | Physic       | Jmecher si Bogat | billgates@gmail.com      | analfabet | Washington | Washington DC | Liberty   | 911    | 0     | 0     | 0         | 0     | +1.673.526.623 | not checked         | not checked             | failed              |


  Scenario Outline: I am testing the bug of authentification
    Given website <registration>
    #When I type <customerRegistration>, <typeOfPerson>, <name>, <email>, <password>, <CUI>, <companyName>, <shire>, <city>, <addressOfSocialSediu>, <regComerOrder>, <delegate>, <IBAN>, <bank>, <telephone>
    And I accept the term and conditions <checkboxTermAndCond> and the details acceptances <checkboxDataProccessing>
    Then my account <creationOfMyAccount> is accepted

    Examples:
      | registration                        | customerRegistration | typeOfPerson | name | email             | password   | CUI        | companyName            | shire | city   | addressOfSocialSediu  | regComerOrder | delegate     | IBAN            | bank     | telephone    | checkboxTermAndCond | checkboxDataProccessing | creationOfMyAccount |
      | https://www.autored.ro/inregistrare | passed               | Juridic      | Siba | sales@sibapack.ro | SuntTeafar | 3103698.67 | SC SIBA PACKAGING SRL | MURES | REGHIN | STR.IERNUTENI, NR.174 | J26/1211/2012 | SIKO NORBERT | RO75BRDE270SV76 | ABN AMRO | 0742.190.240 | checked             | checked                 | passed              |
