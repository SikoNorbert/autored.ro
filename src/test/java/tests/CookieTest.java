package tests;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.Cookies;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CookieTest {
    WebDriver driver;
    String baseURL = "https://www.autored.ro/";
    Cookies cookie;


    @BeforeEach
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/selenium/windows.chrome/chromedriver.exe");
        driver = new ChromeDriver();
        cookie = new Cookies(driver);
        driver.manage().window().maximize();
        driver.get(baseURL);
    }

    @AfterEach
    public void tearDown() {
        WebDriverWait wait = new WebDriverWait(driver, 8);
        driver.quit();
    }


    @Test
    @Order(1)
    public void checkTheCookieEnableSettings() {
        WebElement cookieTitle = driver.findElement(By.id("setaricookies"));
        if(cookie.getCookieDescription().equals("Folosim cookie-uri pentru a-ti oferi cea mai buna experienta pe site-ul nostru. \" +\n" +
                "            \"Poti afla mai multe despre ce cookie-uri folosim sau sa le dezactivezi in setari")){
            Assert.assertEquals(cookieTitle.getText(), cookie.getCookieDescription());
        }

        WebElement cookieAccept = driver.findElement(By.id("acceptcookies"));
        cookieAccept.click();
    }

    @Test
    @Order(2)
    public void pushTheSettingButtonOnCookiePopup(){
        WebDriverWait wait2 = new WebDriverWait(driver, 8);
        WebElement cookieSettings = driver.findElement(By.xpath("/html/body/div[2]/div"));
        cookieSettings.click();
        WebElement popUp = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]"));
        if(popUp.isDisplayed() || popUp.isEnabled()){
            popUp.getText();
        }
        WebElement cookiesFromGoogleAnalyticsCheckButton = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div[5]"));
        if(cookiesFromGoogleAnalyticsCheckButton.isEnabled()){
            cookiesFromGoogleAnalyticsCheckButton.click();
        }
    }
}
