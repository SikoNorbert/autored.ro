package tests;

import io.cucumber.java.eo.Se;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.AuthentificationPage;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Authentification {
    WebDriver driver;
    String baseURL = "https://www.autored.ro/autentificare/";
    AuthentificationPage account;
    String newURL = "https://www.autored.ro/inregistrare";

    @BeforeEach
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/selenium/windows.chrome/chromedriver.exe");
        driver = new ChromeDriver();
        account = new AuthentificationPage(driver);
        driver.manage().window().maximize();
        driver.get(baseURL);
    }

    @AfterEach
    public void tearDown() {
        WebDriverWait wait = new WebDriverWait(driver, 8);
        driver.quit();
    }

    @Test
    @Order(1)
    public void checkThatIAmOnRegistrationPage() {
        driver.navigate().to(baseURL);
        driver.navigate().to(newURL);
        WebElement authentification = driver.findElement(By.xpath("//*[@id=\"header\"]/div[1]/div/div[3]/div[1]"));
        authentification.click();

        WebDriverWait wait2 = new WebDriverWait(driver, 8);
        try {
            WebElement radio = driver.findElement(By.xpath("//*[@id=\"content\"]/article/div/div[1]/div/div[2]/form/div[2]"));
            boolean checked = radio.isSelected();
            if (checked == false)
                radio.click();
        } catch(NoSuchElementException e) {
            System.out.println(e.getMessage());
        }
        WebElement name = driver.findElement(By.name("nume"));
        name.sendKeys("Siba");
        WebElement companyEmail = driver.findElement(By.xpath("//*[@id=\"content\"]/article/div/div[1]/div/div[2]/form/input[4]"));
        companyEmail.sendKeys("sales@sibapack.ro");
        WebElement password = driver.findElement(By.name("parola"));
        password.sendKeys("SuntTeafar");
        WebElement CUI = driver.findElement(By.name("cui"));
        CUI.sendKeys("3103698.67");
        WebElement nameOfCompany = driver.findElement(By.name("nume_companie"));
        nameOfCompany.sendKeys("SC SIBA PACKAGING SRL");
        WebElement locationShire = driver.findElement(By.name("judetpj"));
        Select select = new Select(locationShire);
        select.selectByValue("9");
        WebElement city = driver.findElement(By.name("localitatepj"));
        Select select2 = new Select(city);
        select2.selectByValue("642214818");
        WebElement addressOfSocialSediu = driver.findElement(By.name("adresa_sediu"));
        addressOfSocialSediu.sendKeys("STR.IERNUTENI, NR.174");
        WebElement regComerOrder = driver.findElement(By.name("regcom"));
        regComerOrder.sendKeys("J26/1211/2012");
        WebElement delegate = driver.findElement(By.name("nume_delegat"));
        delegate.sendKeys("SIKO NORBERT");
        WebElement IBAN = driver.findElement(By.name("iban"));
        IBAN.sendKeys("RO75BRDE270SV76");
        WebElement bank = driver.findElement(By.name("banca"));
        Select select3 = new Select(bank);
        select3.selectByValue("33");
        WebElement telephone = driver.findElement(By.name("telefon"));
        telephone.sendKeys("0742.190.240");
        WebElement checkboxTermAndCond = driver.findElement(By.name("acordtermeni"));
        boolean isDisplayTheTerms = checkboxTermAndCond.isSelected();
        if(isDisplayTheTerms == false){
            checkboxTermAndCond.click();
        }
        WebElement checkboxDataProccessing = driver.findElement(By.name("acordpolitica"));
        boolean isDisplayThePoliticalEngagement = checkboxDataProccessing.isSelected();
        if(isDisplayThePoliticalEngagement == false) {
            checkboxDataProccessing.click();
        }
        WebElement creationOfMyAccount = driver.findElement(By.className("addtocartnew"));
        creationOfMyAccount.click();

    }

    @Test
    @Order(2)
    public void checkPhysicPersonRegistrationAccount(){
        driver.navigate().to(newURL);
        WebElement myName = driver.findElement(By.name("nume"));
        myName.sendKeys("Siko Norbert");
        WebElement myEmailAddress = driver.findElement(By.name("email"));
        myEmailAddress.sendKeys("sikonorbert0@gmail.com");
        WebElement password = driver.findElement(By.name("parola"));
        password.sendKeys("123456");
        WebElement mylocation = driver.findElement(By.name("judet"));
        Select selectLocation = new Select(mylocation);
        selectLocation.selectByValue("9");
        WebElement myCity = driver.findElement(By.xpath("//*[@id=\"localitate\"]"));
        Select selectCity = new Select(myCity);
        selectCity.selectByValue("642214818");
        selectCity.selectByVisibleText("Reghin");
        WebElement street = driver.findElement(By.name("str"));
        street.sendKeys("Baii");
        WebElement streetNumber = driver.findElement(By.name("nrstr"));
        streetNumber.sendKeys("22");
        WebElement mobilPhone = driver.findElement(By.name("telefon"));
        mobilPhone.sendKeys("0742.190.240");
        WebElement checkboxTermAndCond = driver.findElement(By.name("acordtermeni"));
        boolean isDisplayTheTerms = checkboxTermAndCond.isSelected();
        if(isDisplayTheTerms == false){
            checkboxTermAndCond.click();
        }
        WebElement checkboxDataProccessing = driver.findElement(By.name("acordpolitica"));
        boolean isDisplayThePoliticalEngagement = checkboxDataProccessing.isSelected();
        if(isDisplayThePoliticalEngagement == false) {
            checkboxDataProccessing.click();
        }
        WebElement sendButton = driver.findElement(By.xpath("//*[@id=\"content\"]/article/div/div[1]/div/div[2]/form/div[4]/input[2]"));
        sendButton.click();
    }
}
