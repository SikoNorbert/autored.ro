package tests;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ConsultancyTest {
    WebDriver driver;
    String baseURL = "https://www.autored.ro/consultantul-tau";

    @BeforeEach
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/selenium/windows.chrome/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(baseURL);
    }

    @AfterEach
    public void tearDown() {
        WebDriverWait wait = new WebDriverWait(driver, 8);
        driver.quit();
    }

    @Test
    @Order(1)
    public void navigateToConsultancyWebPage(){
        driver.navigate().to(baseURL);
    }

    @Test
    @Order(2)
    public void searchByCarBodySeriesThePiecesINeed(){
        WebDriverWait wait2 = new WebDriverWait(driver, 10);
        WebElement inputName = driver.findElement(By.xpath("//*[@id=\"wpcf7-f348-p345-o1\"]/form/div[1]/span/input"));
        inputName.sendKeys("Siko Norbert");
        WebElement inputEmail = driver.findElement(By.xpath("//*[@id=\"wpcf7-f348-p345-o1\"]/form/div[2]/span/input"));
        inputEmail.sendKeys("sikonorbert0@gmail.com");
        WebElement inputCarBodySeries = driver.findElement(By.xpath("//*[@id=\"wpcf7-f348-p345-o1\"]/form/div[4]/span/input"));
        inputCarBodySeries.sendKeys("WBAFF41020L102569");
        WebElement enumerationPieces = driver.findElement(By.xpath("//*[@id=\"wpcf7-f348-p345-o1\"]/form/div[5]/span/textarea"));
        enumerationPieces.sendKeys("Oglinda");
        WebElement checkBox1 = driver.findElement(By.xpath("//*[@id=\"wpcf7-f348-p345-o1\"]/form/div[6]/span/span/span/input"));
        checkBox1.click();
        WebElement checkBox2 = driver.findElement(By.xpath("//*[@id=\"wpcf7-f348-p345-o1\"]/form/div[7]/span/span/span/input"));
        checkBox2.click();
//        WebElement send = driver.findElement(By.xpath("/html/body/div[5]/div[3]/div/div/div/div/article/div/div[4]/div[2]/div/div/form/div[8]/input"));
//        if(send.isSelected()){
//            send.click();
//        } else System.out.println("ERRRRRRORRRRRR!");
        WebElement consultancyTelephoneButton = driver.findElement(By.xpath("//*[@id=\"content\"]/article/div/div[5]/div/div/div/div/div/div[2]/div/div/div"));
        consultancyTelephoneButton.click();
    }
}
