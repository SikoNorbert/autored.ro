package tests;

import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.HomePage;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class HomePage2Test {
    WebDriver driver;
    String baseURL = "https://www.autored.ro/";
    HomePage home;

    @BeforeEach
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/selenium/windows.chrome/chromedriver.exe");
        driver = new ChromeDriver();
        home = new HomePage(driver);
        driver.manage().window().maximize();
        driver.get(baseURL);
    }

    @AfterEach
    public void tearDown() {
        WebDriverWait wait = new WebDriverWait(driver, 8);
        driver.quit();
    }

    @Test
    @Order(1)
    public void checkThePieseUniversaleMenuOnHomePage(){
        home.checkTheMenuTitleInputProductsOnBasket();
    }
}
