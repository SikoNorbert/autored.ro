package tests;

import lombok.var;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.HomePage;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class HomePageTest {
    WebDriver driver;
    String baseURL = "https://www.autored.ro/";
    HomePage home;

    @BeforeEach
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/selenium/windows.chrome/chromedriver.exe");
        driver = new ChromeDriver();
        home = new HomePage(driver);
        driver.manage().window().maximize();
        driver.get(baseURL);
    }

    @AfterEach
    public void tearDown() {
        WebDriverWait wait = new WebDriverWait(driver, 8);
        driver.quit();
    }

    @Test
    @Order(1)
    public void IgetTheWebsiteCurrentUrl() {
        Assert.assertEquals(baseURL, driver.getCurrentUrl());
        Assert.assertTrue(home.checkThatIamOnHomePage());
    }

    @Test
    @Order(2)
    public void IsearchForTheAutoPiecesInTheSearchBar() {
        home.checkTheInputInTheSearchbarAndTheValues();
        WebDriverWait wait = new WebDriverWait(driver, 8);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("butoane-comanda")));
    }

    @Test
    @Order(3)
    public void setTheInformatiiDeFacturare() {
        home.checkTheInputInTheSearchbarAndTheValues();

        String facturareTitle = "Informatii Facturare si Livrare";
        String informationSuggestion = "Pentru a comanda mai mult de un produs sau pentru a comanda pentru o Persoana juridica, va rugam sa adaugati produsul in Cos";

        System.out.println("Nume si prenume \n");
        try {
            WebElement inputFirstNameAndSurName = driver.findElement(By.xpath("//*[@id=\"inregistrare\"]/form/input[3]"));
            inputFirstNameAndSurName.sendKeys("Siko Norbert");
        } catch(ElementNotInteractableException message){
            System.out.println("It is an error?" + message.getMessage());
        }

        System.out.println("Email\n");
        WebElement inputEmail = driver.findElement(By.xpath("//*[@id=\"inregistrare\"]/form/input[4]"));
        inputEmail.sendKeys("norbiateniszes@yahoo.com");

        System.out.println("Informatii livrare\n");

        System.out.println("The shire: ");
        var option = driver.findElement(By.id("judet"));
        Select select = new Select(option);
        select.selectByValue("9");

        System.out.println("The city: ");
        var option2 = driver.findElement(By.id("localitate"));
        Select select2 = new Select(option2);
        select2.selectByValue("642214818");

        System.out.println("The street: ");
        WebElement inputStreetAddress = driver.findElement(By.xpath("//*[@id=\"inregistrare\"]/form/div[2]/div[2]/div[2]/input"));
        inputStreetAddress.sendKeys("Baii");

        System.out.println("The number: ");
        WebElement inputStreetNumber = driver.findElement(By.xpath("//*[@id=\"streetNumberC\"]/input"));
        inputStreetNumber.sendKeys("22");

        System.out.println("The telephone: ");
        WebElement inputPhone = driver.findElement(By.xpath("//*[@id=\"inregistrare\"]/form/div[2]/input[1]"));
        inputPhone.sendKeys("0742.190.240");

        System.out.println("Sunt de acord cu Termenii si conditiile");
        try {
            WebElement checkBox1 = driver.findElement(By.xpath("//*[@id=\"inregistrare\"]/form/div[2]/div[4]/div[1]"));
            checkBox1.click();

            System.out.println("Sunt de acord cu Politica de Prelucrare a Datelor cu Caracter Personal");
            WebElement checkBox2 = driver.findElement(By.xpath("//*[@id=\"inregistrare\"]/form/div[2]/div[4]/div[2]"));
            checkBox2.click();
        } catch (ElementClickInterceptedException click){
            System.out.println(click.getSuppressed());
        }

        WebElement submit = driver.findElement(By.xpath("//*[@id=\"inregistrare\"]/form/div[2]/input[2]"));
        submit.click();

        WebElement X = driver.findElement(By.className("inchide"));
        if (X.isDisplayed()) {
            System.out.println("I uncheck the informatii de facturare webpage");
            X.click();
        }

    }


}
