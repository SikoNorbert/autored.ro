package tests;

import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.ContactPage;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ContactTest {
    WebDriver driver;
    String baseURL = "https://www.autored.ro/contact";
    ContactPage contact;

    @BeforeEach
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/selenium/windows.chrome/chromedriver.exe");
        driver = new ChromeDriver();
        contact = new ContactPage(driver);
        driver.manage().window().maximize();
        driver.get(baseURL);
    }

    @AfterEach
    public void tearDown() {
        WebDriverWait wait = new WebDriverWait(driver, 8);
        driver.quit();
    }

    @Test
    @Order(1)
    public void checkThatIAmOnContactPage(){
        Assert.assertEquals(baseURL, driver.getCurrentUrl());
        contact.setUpTheContactForm();
        Assert.assertTrue(contact.setUpTheContactForm());
    }

    @Test
    @Order(2)
    public void contactDate(){
        contact.inputMyBeneficiaryContactInformationForTheContactForm();
    }

    @Test
    @Order(3)
    public void sendInfosToContactPage(){
        contact.sendInformationsToContactForm();
    }

}
