package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Autored {
    private String website;
    private String title;
    private String logo;
    private String jumbotron;
    private int autoredId;

    public Autored(){
    }
}
