package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthentificationModel {
    private String databaseName;
    private String registration;
    private String customerRegistration;
    private String typeOfPerson;
    private String name;
    private String email;
    private String password;
    private String shire;
    private String city;
    private String street;
    private float number;
    private int block;
    private int scale;
    private int apartment;
    private int floor;
    private String mobilphone;
    private String checkboxTermAndCond;
    private String checkboxDataProccessing;
    private String creationOfMyAccount;
    private String CUI;
    private String companyName;
    private String addressOfSocialSediu;
    private String regComerOrder;
    private String delegate;
    private String IBAN;
    private String bank;
    private String telephone;

    public AuthentificationModel(String AutoredDatabase){
        this.databaseName = AutoredDatabase;
    }
}
