package model;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
public class MenuSection {
    private String menuSection;
    private String AutoredWebsite;
    private String listOfProducts;
    private String products;
    private int price;
    private boolean productCorrectness;

    public MenuSection(String products, boolean productCorrectness){
        this.products = products;
        this.productCorrectness = productCorrectness;
    }

    public HashMap<String, String> addCopyOfMenuSectionAndWebsitePages(){
        HashMap<String, String> addTwoCategories = new HashMap<>();
        addTwoCategories.put("PIESE UNIVERSALE","https://www.autored.ro");
        addTwoCategories.put("ULEIURI MOTOR","https://www.autored.ro/articole/54/uleiuri-motor");
        addTwoCategories.put("Unelte și scule","https://www.autored.ro/articole/28/cricuri");
        addTwoCategories.put("Întreținere auto","https://www.autored.ro/articole/41/accesorii-cur%C4%83%C8%9Bare-%C8%99i-%C3%AEntre%C8%9Binere");
        addTwoCategories.put("Accesorii auto interior","https://www.autored.ro/articole/13/covora%C8%99e-auto");
        addTwoCategories.put("Accesorii auto exterior","https://www.autored.ro/articole/5/%C8%98terg%C4%83toare");
        addTwoCategories.put("Accesorii de siguranță în trafic","https://www.autored.ro/articole/18/accesorii-siguran%C8%9B%C4%83-%C3%AEn-trafic");

        return addTwoCategories;
    }

    public void addProductsToTheList() {
        HashSet<String> productList = new HashSet<String>();
        productList.add("ULEI DE MOTOR 8100 X-CLEAN+ 5W30 5L-MOTUL");
        productList.add("ULEI DE MOTOR 8100 X-CESS 5W40 5L-MOTUL");
        productList.add("ULEI DE MOTOR MAGNATEC DIESEL 5W40 5L-CASTROL");
        productList.add("ULEI DE MOTOR EDGE 5W30 LL 5L-CASTROL");
        productList.add("ULEI DE MOTOR 4100 TURBOLIGHT 10W40 1L-MOTUL");
        productList.add("CRIC CROCODIL 2T (TUV-GS.CE)- FASTR");
        productList.add("CRIC HIDRAULIC 10 TONE (TUV-GS.CE)");
        productList.add("CRIC HIDRAULIC 10T-JBM");
        productList.add("CRIC HIDRAULIC 12 TONE (TUV-GS.CE)- FASTR");
        productList.add("CRIC HIDRAULIC 16 TONE (TUV-GS.CE)- FASTR");
        productList.add("CRIC HIDRAULIC 4 TONE (TUV-GS.CE)- FASTR");
        productList.add("CRIC HIDRAULIC IN CUTIE DIN PLASTIC 2T.");
        productList.add("CRIC HIDRAULIC IN CUTIE DIN PLASTIC 3T");
        productList.add("CRIC HIDRAULIC IN CUTIE DIN PLASTIC 5T");
        productList.add("CRIC MECANIC 1.5 T ALCA");
        productList.add("CRIC MECANIC 1T (TUV-GS.CE)");
        productList.add("CRIC MECANIC 2T ALCA");
        productList.add("SAMPON AUTO CU CEARA 1000 ML-MANNOL");
        productList.add("SOLUTIE LUSTRUIT BORD XXL 600 ML");
        productList.add("SOLUTIE INTRETINERE TAPITERIE PIELE 600 ML");
        productList.add("SPRAY CU SPUMA PENTRU CURATAREA TAPITERIEI TEXTILE 400 ML SONAX");
        productList.add("SOLUTIE LUSTRUIT BORD 200 ML");
        productList.add("SPRAY PROTECTIE SI INTRETINERE BORD (VANILIE) 250 ML-MANNOL");
        productList.add("LAVETA PENTRU SUPRAFETE EXTERIOARE SONAX");
        productList.add("SOLUTIE INTRETINERE BORD-VANILIE 300 ML");
        productList.add("AIR PURIFIER- SPRAY MOLECULAR PENTRU ELIMINAREA MIROSURILOR");
        productList.add("PULVERIZATOR SOLUTIE DE CURATAT UNIVERSALA 500ML");
        productList.add("LAVETA PIELE CHAMOIS");
        productList.add("BURETE INDEPARTARE REZIDUURI INSECTE");
        productList.add("KIT REPARATII JANTE DE ALIAJ-MANNOL");
        productList.add("SOLUTIE INTRETINERE BORD-CITRICE 300 ML");
        productList.add("SPRAY PROTECTIE SI INTRETINERE BORD (NEW CAR) 250 ML-MANNOL");
        productList.add("SPRAY CURATITOR BORD CU EFECT MAT 100 ML-MANNOL");
        productList.add("SPRAY PROTECTIE SI INTRETINERE BORD (MAR) 250 ML-MANNOL");
        productList.add("PERIE PLASTIC RACORDABILA CU REGLAJ DEBIT");
        productList.add("PERIE PENTRU SPALAT CU MANER TELESCOPIC- MEGA DRIVE");
        productList.add("SET COVORASE AUTO CAUCIUC BMW X3 (F25) (2010-2017)/X4 (F26) (2014-2018) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC AUDI A6 (C6) (2004-2006) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC AUDI A6/A7 (C7) (2011-2018) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC AUDI Q7 (2005-2015) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC BMW 3er (E 36) (1992-1998) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC BMW 3er (E 46) (1998-2004) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC BMW 3er (E 90) (2004-2013) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC BMW 3er (F30) (2012-)/ 3er (G20) (2019-)/ 4er (F32) (2013-) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC BMW 5er (E 39) (1996-2004) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC AUDI A5 (B8) (2007-2016) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC BMW 5er (F10/F11) (2010-2017) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC BMW X5 (E70) (2007-2013)/X5 (F15) (2013-2018)/X6(F16) (2015-2019) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC DACIA LOGAN (2012-)/SANDERO (2013-)/DOKKER(2013-)/LODGY (2013-) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC DAEWOO MATIZ (1998-2005) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC FIAT DOBLO I (2000-2009) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC FORD FOCUS (2001-2006) - UMBRELLA");
        productList.add("SET COVORASE MARGINE ROSIE 68x48/48x47 CM FASTR");
        productList.add("SET COVORASE AUTO PVC ARIZONA NEGRU SET 2 BUC UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA VW GOLF IV / BORA / NEW BEETLE / AUDI A3 / SEAT LEON I / TOLEDO II / SKODA OCT I - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA DACIA LOGAN I(2004-2012) SANDERO I(2008 - 2012) DUSTER(2010 - 2018)-4 PCS - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA DACIA LOGAN I(2004-2012) DUSTER I(2010 - 2018) SANDERO I(2008-2012) - 5PCS - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA SEAT CORDOBA (2002-2008).IBIZA (2002-2008) VW POLO (2002-2009). SKODA FABIA (1999-2007). ROOMSTER (2006-2015) - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA OPEL ASTRA G (1998-2004) - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA RENAULT MEGANE II (2002-2009) - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA DACIA SANDERO I (2008-2012) - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA VW CADDY III(2003-2020)/GOLF V(2003-2009)/GOLF VI(2008-2016)/JETTA V(2005-2011) AUDI A3(2003-2013) SEAT LEON II(2005-2012)/TOLEDO III(2004-2009) SKODA OCT II (2004-2013) - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA VW T4 (1990-2003) - 2PCS - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA SKODA SUPERB I(2001-2008) VW PASSAT B5(1996-2005) - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA MERCEDES SPRINTER (2006-2018). VW CRAFTER (2006-2016) - 2PCS - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA FORD FOCUS I(1998 -2004) FOCUS II(2004 - 2011) FIESTA VII(2008 - 2017) - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA BMW 3 E90/E91 (2004-2011) - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA VW PASSAT B6(2005-2010)/B7(2010-2014) - UMBRELLA");
        productList.add("SET COVORASE AUTO TAVITA BMW 3 E46 (1998-2005) - UMBRELLA");
        productList.add("SET COVORASE AUTO CAUCIUC AUDI A4 (B5) (1995-2000) - UMBRELLA");
        productList.add("STERGATOR 20");
        productList.add("STERGATOR ALCA SUPER FLAT 24`/60 cm");
        productList.add("STERGATOR ALCA SUPER FLAT 28`/70 cm");
        productList.add("SET STERGATOARE 2 BUC (CAUC) MGN. II - RENAULT");
        productList.add("SET STERGATOARE 2BUC - RENAULT");
        productList.add("STERGATOR PARBRIZ - RENAULT");
        productList.add("STERGATOR 16\"/40 cm KONTAKT SPECIAL");
        productList.add("STERGATOR 18/45 KONTAKT SPECIAL");
        productList.add("STERGATOR 19/48 KONTAKT SPECIAL");
        productList.add("STERGATOR ALCA SUPER FLAT 21`/53 cm");
        productList.add("STERGATOR 21/53 KONTAKT SPECIAL");
        productList.add("STERGATOR 22''/56 CM KONTAKT SPECIAL");
        productList.add("STERGATOR 24\"/60 cm KONTAKT SPECIAL");
        productList.add("STERGATOR 26\"/65 cm KONTAKT SPECIAL");
        productList.add("STERGATOR 28\"/70 cm KONTAKT SPECIAL");
        productList.add("STERGATOR CAMION 24\"/60 cm KONTAKT SPECIAL");
        productList.add("STERGATOR CAMION 26\"/65 cm KONTAKT SPECIAL");
        productList.add("STERGATOR CAMION 28\"/71 cm KONTAKT SPECIAL");
        productList.add("STERGATOR 20``/50 cm - ALCA");
        productList.add("STERGATOR ALL SEASONS 26/65 cm - MEGA DRIVE");
        productList.add("STERGATOR ALCA SUPER FLAT 15`/38 cm - ALCA");
        productList.add("STERGATOR 16`/40 cm -ALCA");
        productList.add("STERGATOR 15'' /38 cm - ALCA");
        productList.add("STERGATOR 22``/56 cm - ALCA");
        productList.add("STERGATOR 18`/45 cm - ALCA");
        productList.add("STERGATOR 19``/48 cm - ALCA");
        productList.add("STERGATOR 21``/53 cm - ALCA");
        productList.add("STERGATOR 24``/60 cm - ALCA");
        productList.add("STERGATOR ALL SEASONS 28/70 cm - MEGA DRIVE");
        productList.add("STERGATOR ALCA SUPER FLAT 18`/45 cm");
        productList.add("STERGATOR ALCA SUPER FLAT 19`/48 cm");
        productList.add("STERGATOR ALCA SUPER FLAT 20`/50m");
        productList.add("KIT TRIUNGHI SI TRUSA MEDICALA");
        productList.add("KIT TRUSA MEDICALA, VESTA SI TRIUNGHI");
        productList.add("TRUSA MEDICALA");
        productList.add("VESTA REFLECTORIZANTA 60G");
        productList.add("STINGATOR REINCARCABIL 1KG CU MANOMETRU");
        productList.add("STINGATOR TIP SPRAY 400ML");

        Iterator<String> it = productList.iterator();
        String element;
        while (it.hasNext()) {
            element = it.next();
            System.out.println(element);
        }
        System.out.println(productList.size());

//        if(productList == null){
//            return;
//        }
//        System.out.println(productList.stream().peek((Consumer<? super String>) productList.stream().sorted()));
    }

    public void addPriceToTheList() {
        LinkedList<Integer> prices = new LinkedList<Integer>();
        prices.add(200);
        prices.add(166);
        prices.add(147);
        prices.add(191);
        prices.add(28);
        prices.add(140);
        prices.add(104);
        prices.add(178);
        prices.add(124);
        prices.add(140);
        prices.add(71);
        prices.add(97);
        prices.add(112);
        prices.add(142);
        prices.add(84);
        prices.add(63);
        prices.add(109);
        prices.add(17);
        prices.add(33);
        prices.add(31);
        prices.add(31);
        prices.add(21);
        prices.add(19);
        prices.add(13);
        prices.add(22);
        prices.add(37);
        prices.add(11);
        prices.add(12);
        prices.add(8);
        prices.add(59);
        prices.add(22);
        prices.add(20);
        prices.add(12);
        prices.add(19);
        prices.add(12);
        prices.add(119);
        prices.add(163);
        prices.add(134);
        prices.add(147);
        prices.add(160);
        prices.add(132);
        prices.add(159);
        prices.add(144);
        prices.add(151);
        prices.add(163);
        prices.add(127);
        prices.add(154);
        prices.add(161);
        prices.add(132);
        prices.add(132);
        prices.add(133);
        prices.add(144);
        prices.add(47);
        prices.add(45);
        prices.add(104);
        prices.add(91);
        prices.add(109);
        prices.add(105);
        prices.add(111);
        prices.add(114);
        prices.add(95);
        prices.add(104);
        prices.add(82);
        prices.add(118);
        prices.add(78);
        prices.add(103);
        prices.add(101);
        prices.add(104);
        prices.add(104);
        prices.add(153);
        prices.add(12);
        prices.add(23);
        prices.add(33);
        prices.add(111);
        prices.add(93);
        prices.add(43);
        prices.add(10);
        prices.add(11);
        prices.add(11);
        prices.add(22);
        prices.add(12);
        prices.add(13);
        prices.add(16);
        prices.add(18);
        prices.add(19);
        prices.add(20);
        prices.add(21);
        prices.add(23);
        prices.add(12);
        prices.add(11);
        prices.add(18);
        prices.add(10);
        prices.add(10);
        prices.add(12);
        prices.add(11);
        prices.add(12);
        prices.add(12);
        prices.add(13);
        prices.add(8);
        prices.add(20);
        prices.add(20);
        prices.add(21);
        prices.add(56);
        prices.add(65);
        prices.add(39);
        prices.add(7);
        prices.add(53);
        prices.add(26);

        Iterator<Integer> integ = prices.iterator();
        int insert;
        while (integ.hasNext()) {
            insert = integ.next();
            System.out.println(insert);
        }
        System.out.println(prices.size());

//        System.out.println(prices.stream().peek((Consumer<? super Integer>) prices.stream().sorted()));
    }
}


