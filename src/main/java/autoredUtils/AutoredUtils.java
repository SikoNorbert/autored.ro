package autoredUtils;

import model.Autored;

import java.util.LinkedList;

public abstract class AutoredUtils extends Autored {

    private static LinkedList<String> searchBar;

    public LinkedList<String> getSearchBar() {
        return searchBar;
    }

    public void setSearchBar(LinkedList<String> searchBar) {
        this.searchBar = searchBar;
    }

    public static LinkedList<String> addToSearchBar(){
        searchBar = new LinkedList<>();
        searchBar.add("Far");
        searchBar.add("Mecanism Actionare Geam ORIGINAL ERSATZTEIL");
        searchBar.add("Ulei Cutie Viteze Manuala Classic SAE 90");
        searchBar.add("Ulei Diferential HYPO S LS75W140");
        searchBar.add("Set Rulment Roata FAG SmartSET");
        searchBar.add("Ulei Cutie Viteze Manuala Classic SAE 90");
        searchBar.add("Siguranta Fuzibila");
        searchBar.add("Termostat,lichid Racire");
        searchBar.add("Rola Intinzator,curea Transmisie");
        searchBar.add( "Ulei De Motor Special Tec AA 5W-30");
        searchBar.add("Suport, Flanșă De Acționare Alternator Brand new | AS-PL | Alternator s.r.e. brackets");
        searchBar.add("Colector, Alternator Brand new | AS-PL | Alternator slip rings");
        searchBar.add("Ulei De Motor Special Tec AA 5W-40 Diesel");
        return searchBar;
    }
}
