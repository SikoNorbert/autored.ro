package autoredUtils;

import model.MenuSection;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class MenuUtils {

    public static HashMap<String, String> menus = new HashMap<>();
    public static HashMap<String, Boolean> productsByReservation = new HashMap<>();
    Iterator<String> it;
    MenuSection[] menu = new MenuSection[]{
            new MenuSection("PIESE UNIVERSALE", false),
            new MenuSection("ULEIURI MOTOR", true),
            new MenuSection("Unelte și scule", true),
            new MenuSection("Întreținere auto", true),
            new MenuSection("Accesorii auto interior", true),
            new MenuSection("Accesorii auto exterior", true),
            new MenuSection("Accesorii de siguranță în trafic", true)

    };

    public static HashMap<String, String> addMenuSectionForTheAutoredWebsite() {

        menus.put("PIESE UNIVERSALE", "https://www.autored.ro");
        menus.put("ULEIURI MOTOR", "https://www.autored.ro/articole/54/uleiuri-motor");
        menus.put("Unelte și scule", "https://www.autored.ro/articole/28/cricuri");
        menus.put("Întreținere auto", "https://www.autored.ro/articole/41/accesorii-cur%C4%83%C8%9Bare-%C8%99i-%C3%AEntre%C8%9Binere");
        menus.put("Accesorii auto interior", "https://www.autored.ro/articole/13/covora%C8%99e-auto");
        menus.put("Accesorii auto exterior", "https://www.autored.ro/articole/5/%C8%98terg%C4%83toare");
        menus.put("Accesorii de siguranță în trafic", "https://www.autored.ro/articole/18/accesorii-siguran%C8%9B%C4%83-%C3%AEn-trafic");

        System.out.println(menus);

        return menus;
    }


    public static HashMap<String, Boolean> chooseTheProducts() {
        productsByReservation.put("-", false);
        productsByReservation.put("ULEI DE MOTOR 8100 X-CESS 5W40 5L-MOTUL", true);
        productsByReservation.put("ULEI DE MOTOR MAGNATEC DIESEL 5W40 5L-CASTROL", true);
        productsByReservation.put("ULEI DE MOTOR EDGE 5W30 LL 5L-CASTROL", true);
        productsByReservation.put("ULEI DE MOTOR 4100 TURBOLIGHT 10W40 1L-MOTUL", true);
        productsByReservation.put("CRIC CROCODIL 2T (TUV-GS.CE)- FASTR", true);

        System.out.println(productsByReservation);

        return productsByReservation;
    }

    public static List<MenuSection> addProductReservation() {
        MenuSection[] products = new MenuSection[]{
                new MenuSection("-", false),
                new MenuSection("ULEI DE MOTOR 8100 X-CLEAN+ 5W30 5L-MOTUL",true),
                new MenuSection("ULEI DE MOTOR 8100 X-CESS 5W40 5L-MOTUL", true),
                new MenuSection("ULEI DE MOTOR MAGNATEC DIESEL 5W40 5L-CASTROL", true),
                new MenuSection("ULEI DE MOTOR EDGE 5W30 LL 5L-CASTROL",true),
                new MenuSection("ULEI DE MOTOR 4100 TURBOLIGHT 10W40 1L-MOTUL",true),
                new MenuSection("CRIC CROCODIL 2T (TUV-GS.CE)- FASTR",true),
                new MenuSection("CRIC HIDRAULIC 10 TONE (TUV-GS.CE)",true),
                new MenuSection("CRIC HIDRAULIC 10T-JBM",true),
                new MenuSection("CRIC HIDRAULIC 12 TONE (TUV-GS.CE)- FASTR",true),
                new MenuSection("CRIC HIDRAULIC 16 TONE (TUV-GS.CE)- FASTR",true),
                new MenuSection("CRIC HIDRAULIC 4 TONE (TUV-GS.CE)- FASTR",true),
                new MenuSection("CRIC HIDRAULIC IN CUTIE DIN PLASTIC 2T.",true),
                new MenuSection("CRIC HIDRAULIC IN CUTIE DIN PLASTIC 3T",true),
                new MenuSection("CRIC HIDRAULIC IN CUTIE DIN PLASTIC 5T",true),
                new MenuSection("CRIC MECANIC 1.5 T ALCA",true),
                new MenuSection("CRIC MECANIC 1T (TUV-GS.CE)",true),
                new MenuSection("CRIC MECANIC 2T ALCA",true),
                new MenuSection("SAMPON AUTO CU CEARA 1000 ML-MANNOL",true),
                new MenuSection("SOLUTIE LUSTRUIT BORD XXL 600 ML",true),
                new MenuSection("SOLUTIE INTRETINERE TAPITERIE PIELE 600 ML",true),
                new MenuSection("SPRAY CU SPUMA PENTRU CURATAREA TAPITERIEI TEXTILE 400 ML SONAX",true),
                new MenuSection("SOLUTIE LUSTRUIT BORD 200 ML",true),
                new MenuSection("SPRAY PROTECTIE SI INTRETINERE BORD (VANILIE) 250 ML-MANNOL",true),
                new MenuSection("LAVETA PENTRU SUPRAFETE EXTERIOARE SONAX",true),
                new MenuSection("SOLUTIE INTRETINERE BORD-VANILIE 300 ML",true),
                new MenuSection("AIR PURIFIER- SPRAY MOLECULAR PENTRU ELIMINAREA MIROSURILOR",true),
                new MenuSection("PULVERIZATOR SOLUTIE DE CURATAT UNIVERSALA 500ML",true),
                new MenuSection("LAVETA PIELE CHAMOIS",true),
                new MenuSection("BURETE INDEPARTARE REZIDUURI INSECTE",true),
                new MenuSection("KIT REPARATII JANTE DE ALIAJ-MANNOL",true),
                new MenuSection("SOLUTIE INTRETINERE BORD-CITRICE 300 ML",true),
                new MenuSection("SPRAY PROTECTIE SI INTRETINERE BORD (NEW CAR) 250 ML-MANNOL",true),
                new MenuSection("SPRAY CURATITOR BORD CU EFECT MAT 100 ML-MANNOL",true),
                new MenuSection("SPRAY PROTECTIE SI INTRETINERE BORD (MAR) 250 ML-MANNOL",true),
                new MenuSection("PERIE PLASTIC RACORDABILA CU REGLAJ DEBIT",true),
                new MenuSection("PERIE PENTRU SPALAT CU MANER TELESCOPIC- MEGA DRIVE",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC BMW X3 (F25) (2010-2017)/X4 (F26) (2014-2018) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC AUDI A6 (C6) (2004-2006) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC AUDI A6/A7 (C7) (2011-2018) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC AUDI Q7 (2005-2015) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC BMW 3er (E 36) (1992-1998) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC BMW 3er (E 46) (1998-2004) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC BMW 3er (E 90) (2004-2013) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC BMW 3er (F30) (2012-)/ 3er (G20) (2019-)/ 4er (F32) (2013-) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC BMW 5er (E 39) (1996-2004) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC AUDI A5 (B8) (2007-2016) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC BMW 5er (F10/F11) (2010-2017) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC BMW X5 (E70) (2007-2013)/X5 (F15) (2013-2018)/X6(F16) (2015-2019) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC DACIA LOGAN (2012-)/SANDERO (2013-)/DOKKER(2013-)/LODGY (2013-) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC DAEWOO MATIZ (1998-2005) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC FIAT DOBLO I (2000-2009) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC FORD FOCUS (2001-2006) - UMBRELLA",true),
                new MenuSection("SET COVORASE MARGINE ROSIE 68x48/48x47 CM FASTR",true),
                new MenuSection("SET COVORASE AUTO PVC ARIZONA NEGRU SET 2 BUC UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA VW GOLF IV / BORA / NEW BEETLE / AUDI A3 / SEAT LEON I / TOLEDO II / SKODA OCT I - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA DACIA LOGAN I(2004-2012) SANDERO I(2008 - 2012) DUSTER(2010 - 2018)-4 PCS - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA DACIA LOGAN I(2004-2012) DUSTER I(2010 - 2018) SANDERO I(2008-2012) - 5PCS - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA SEAT CORDOBA (2002-2008).IBIZA (2002-2008) VW POLO (2002-2009). SKODA FABIA (1999-2007). ROOMSTER (2006-2015) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA OPEL ASTRA G (1998-2004) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA RENAULT MEGANE II (2002-2009) - UMBRELLA",false),
                new MenuSection("SET COVORASE AUTO TAVITA DACIA SANDERO I (2008-2012) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA VW CADDY III(2003-2020)/GOLF V(2003-2009)/GOLF VI(2008-2016)/JETTA V(2005-2011) AUDI A3(2003-2013) SEAT LEON II(2005-2012)/TOLEDO III(2004-2009) SKODA OCT II (2004-2013) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA VW T4 (1990-2003) - 2PCS - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA SKODA SUPERB I(2001-2008) VW PASSAT B5(1996-2005) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA MERCEDES SPRINTER (2006-2018). VW CRAFTER (2006-2016) - 2PCS - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA FORD FOCUS I(1998 -2004) FOCUS II(2004 - 2011) FIESTA VII(2008 - 2017) - UMBRELLA",false),
                new MenuSection("SET COVORASE AUTO TAVITA BMW 3 E90/E91 (2004-2011) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA VW PASSAT B6(2005-2010)/B7(2010-2014) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO TAVITA BMW 3 E46 (1998-2005) - UMBRELLA",true),
                new MenuSection("SET COVORASE AUTO CAUCIUC AUDI A4 (B5) (1995-2000) - UMBRELLA",true),
                new MenuSection("STERGATOR 20",true),
                new MenuSection("STERGATOR ALCA SUPER FLAT 24`/60 cm",true),
                new MenuSection("STERGATOR ALCA SUPER FLAT 28`/70 cm",true),
                new MenuSection("SET STERGATOARE 2 BUC (CAUC) MGN. II - RENAULT",true),
                new MenuSection("SET STERGATOARE 2BUC - RENAULT",true),
                new MenuSection("STERGATOR PARBRIZ - RENAULT",true),
                new MenuSection("STERGATOR 16\"/40 cm KONTAKT SPECIAL",true),
                new MenuSection("STERGATOR 18/45 KONTAKT SPECIAL",true),
                new MenuSection("STERGATOR 19/48 KONTAKT SPECIAL",true),
                new MenuSection("STERGATOR ALCA SUPER FLAT 21`/53 cm",true),
                new MenuSection("STERGATOR 21/53 KONTAKT SPECIAL",true),
                new MenuSection("STERGATOR 22''/56 CM KONTAKT SPECIAL",false),
                new MenuSection("STERGATOR 24\"/60 cm KONTAKT SPECIAL",false),
                new MenuSection("STERGATOR 26\"/65 cm KONTAKT SPECIAL",false),
                new MenuSection("STERGATOR 28\"/70 cm KONTAKT SPECIAL",false),
                new MenuSection("STERGATOR CAMION 24\"/60 cm KONTAKT SPECIAL",false),
                new MenuSection("STERGATOR CAMION 26\"/65 cm KONTAKT SPECIAL",false),
                new MenuSection("STERGATOR CAMION 28\"/71 cm KONTAKT SPECIAL",false),
                new MenuSection("STERGATOR 20``/50 cm - ALCA",false),
                new MenuSection("STERGATOR ALL SEASONS 26/65 cm - MEGA DRIVE",false),
                new MenuSection("STERGATOR ALCA SUPER FLAT 15`/38 cm - ALCA",false),
                new MenuSection("STERGATOR 16`/40 cm -ALCA",true),
                new MenuSection("STERGATOR 15'' /38 cm - ALCA",true),
                new MenuSection("STERGATOR 22``/56 cm - ALCA",true),
                new MenuSection("STERGATOR 18`/45 cm - ALCA",true),
                new MenuSection("STERGATOR 19``/48 cm - ALCA",false),
                new MenuSection("STERGATOR 21``/53 cm - ALCA",true),
                new MenuSection("STERGATOR 24``/60 cm - ALCA",false),
                new MenuSection("STERGATOR ALL SEASONS 28/70 cm - MEGA DRIVE",true),
                new MenuSection("STERGATOR ALCA SUPER FLAT 18`/45 cm",true),
                new MenuSection("STERGATOR ALCA SUPER FLAT 19`/48 cm",true),
                new MenuSection("STERGATOR ALCA SUPER FLAT 20`/50m",true),
                new MenuSection("KIT TRIUNGHI SI TRUSA MEDICALA",true),
                new MenuSection("KIT TRUSA MEDICALA, VESTA SI TRIUNGHI",true),
                new MenuSection("TRUSA MEDICALA",true),
                new MenuSection("VESTA REFLECTORIZANTA 60G",true),
                new MenuSection("STINGATOR REINCARCABIL 1KG CU MANOMETRU",true),
                new MenuSection("STINGATOR TIP SPRAY 400ML",true)
        };
           List<MenuSection> allProducts = Stream.of(MenuUtils.addProductReservation())
                   .flatMap(Collection::stream)
                   .collect(Collectors.toList());

        System.out.println(allProducts);
           return allProducts;
    }
}
