package autoredUtils;


import model.AuthentificationModel;

import java.util.LinkedList;

public abstract class AuthentificationUtils {

    public static LinkedList<AuthentificationModel> addAllObjects(){
        LinkedList<AuthentificationModel> authentification = new LinkedList<>();
        AuthentificationModel setUp = new AuthentificationModel("Autored database registration page");
        AuthentificationModel setUp2 = new AuthentificationModel("Autored database registration page");
        AuthentificationModel setUp3 = new AuthentificationModel("Autored database registration page");
        AuthentificationModel setUp4 = new AuthentificationModel("Autored database registration page");
        AuthentificationModel setUp5 = new AuthentificationModel("Autored database registration page");

        setUp.setDatabaseName("Autored.ro");
        setUp.setRegistration("https://www.autored.ro/inregistrare");
        setUp.setCustomerRegistration("passed");
        setUp.setTypeOfPerson("Physic");
        setUp.setName("Siko Norbert");
        setUp.setEmail("norbiateniszes@yahoo.com");
        setUp.setPassword("123456");
        setUp.setShire("MURES");
        setUp.setCity("REGHIN");
        setUp.setStreet("Baii");
        setUp.setNumber(22);
        setUp.setBlock(0);
        setUp.setScale(0);
        setUp.setApartment(0);
        setUp.setFloor(0);
        setUp.setMobilphone("0742.190.240");
        setUp.setCheckboxTermAndCond("checked");
        setUp.setCheckboxDataProccessing("checked");
        setUp.setCreationOfMyAccount("passed");

        setUp2.setDatabaseName("Autored.ro");
        setUp2.setRegistration("https://www.autored.ro/inregistrare");
        setUp2.setCustomerRegistration("failed");
        setUp2.setTypeOfPerson("Physic");
        setUp2.setName("Alex Fierascu");
        setUp2.setEmail("ioan.alexf@gmail.com");
        setUp2.setPassword("abcdef");
        setUp2.setShire("IASI");
        setUp2.setCity("IASI");
        setUp2.setStreet("Bogatilor");
        setUp2.setNumber(9);
        setUp2.setBlock(0);
        setUp2.setScale(0);
        setUp2.setApartment(0);
        setUp2.setFloor(0);
        setUp2.setMobilphone("0755.321.999");
        setUp2.setCheckboxTermAndCond("not checked");
        setUp2.setCheckboxDataProccessing("not checked");
        setUp2.setCreationOfMyAccount("failed");

        setUp3.setDatabaseName("Autored.ro");
        setUp3.setRegistration("https://www.autored.ro/inregistrare");
        setUp3.setCustomerRegistration("passed");
        setUp3.setTypeOfPerson("Physic");
        setUp3.setName("Siko J Norbert");
        setUp3.setEmail("sikonorbert0@gmail.com");
        setUp3.setPassword("prostul");
        setUp3.setShire("MURES");
        setUp3.setCity("MURES");
        setUp3.setStreet("Iernuteni");
        setUp3.setNumber(175);
        setUp3.setBlock(0);
        setUp3.setScale(0);
        setUp3.setApartment(0);
        setUp3.setFloor(0);
        setUp3.setMobilphone("0742.190.240");
        setUp3.setCheckboxTermAndCond("checked");
        setUp3.setCheckboxDataProccessing("checked");
        setUp3.setCreationOfMyAccount("passed");

        setUp4.setDatabaseName("Autored.ro");
        setUp4.setRegistration("https://www.autored.ro/inregistrare");
        setUp4.setCustomerRegistration("failed");
        setUp4.setTypeOfPerson("Physic");
        setUp4.setName("Jmecher si Bogat");
        setUp4.setEmail("billgates@gmail.com");
        setUp4.setPassword("analfabet");
        setUp4.setShire("Washington");
        setUp4.setCity("Washington DC");
        setUp4.setStreet("Liberty");
        setUp4.setNumber(911);
        setUp4.setBlock(0);
        setUp4.setScale(0);
        setUp4.setApartment(0);
        setUp4.setFloor(0);
        setUp4.setMobilphone("+1.673.526.623");
        setUp4.setCheckboxTermAndCond("not checked");
        setUp4.setCheckboxDataProccessing("not checked");
        setUp4.setCreationOfMyAccount("failed");

        authentification.add(setUp);
        authentification.add(setUp2);
        authentification.add(setUp3);
        authentification.add(setUp4);
        authentification.add(setUp5);

        return authentification;
    }

    public static LinkedList<AuthentificationModel> addJuridicPersonToTheAccountRegistration(){
        LinkedList<AuthentificationModel> authentification2 = new LinkedList<>();

        AuthentificationModel setUpJuridic = new AuthentificationModel("Autored database registration page");

        setUpJuridic.setRegistration("https://www.autored.ro/inregistrare");
        setUpJuridic.setCustomerRegistration("passed");
        setUpJuridic.setTypeOfPerson("Juridic");
        setUpJuridic.setName("Siba");
        setUpJuridic.setEmail("sales@sibapack.ro");
        setUpJuridic.setPassword("SuntTeafar");
        setUpJuridic.setCUI("3103698.67");
        setUpJuridic.setCompanyName("SC SIBA PACKAGING SRL");
        setUpJuridic.setShire("MURES");
        setUpJuridic.setCity("REGHIN");
        setUpJuridic.setAddressOfSocialSediu("STR.IERNUTENI, NR.174");
        setUpJuridic.setRegComerOrder("J26/1211/2012");
        setUpJuridic.setDelegate("SIKO NORBERT");
        setUpJuridic.setIBAN("RO75BRDE270SV76");
        setUpJuridic.setBank("ABN AMRO");
        setUpJuridic.setTelephone("0742.190.240");
        setUpJuridic.setCheckboxTermAndCond("checked");
        setUpJuridic.setCheckboxDataProccessing("checked");
        setUpJuridic.setCreationOfMyAccount("passed");

        authentification2.add(setUpJuridic);

        return authentification2;
    }
}

