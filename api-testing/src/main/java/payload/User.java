package payload;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class User {
    @JsonProperty("USER_ID")
    private String USER_ID;

    @JsonProperty("ACCOUNT_ID")
    private String ACCOUNT_ID;

    @JsonProperty("NAME")
    private String NAME;

    @JsonProperty("SHORT_NAME")
    private String SHORT_NAME;

    @JsonProperty("IS_BUSINESS_PERSON_ACCOUNT")
    private boolean S_BUSINESS_PERSON_ACCOUNT;

    @JsonProperty("HAS_SECONDARY_BUSINESS_PERSON")
    private boolean HAS_SECONDARY_BUSINESS_PERSON;

    @JsonProperty("IS_MESSENGER_ONLY_USER")
    private boolean IS_MESSENGER_ONLY_USER;
}
