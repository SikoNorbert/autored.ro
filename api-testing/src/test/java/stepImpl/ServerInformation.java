package stepImpl;

import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class ServerInformation {
    String baseURL = "https://api.whatsapp.com/";
    String userEndPoint = "send/?phone=40786113308&text&app_absent=0";
    private RequestSpecification requestSpecification = RestAssured.given().baseUri(baseURL).basePath(userEndPoint).contentType(ContentType.JSON);

    private ResponseSpecBuilder responseSpecBuilder;
    private ResponseSpecification responseSpecification;

    @Test
    public void checkServerInformation() {
        responseSpecBuilder = new ResponseSpecBuilder();
        responseSpecBuilder.expectHeader("Vary", "Accept-Encoding");
        responseSpecBuilder.expectHeader("Pragma", "no-cache");
        responseSpecBuilder.expectHeader("Cache-Control", "private, no-cache, no-store, must-revalidate");
        responseSpecBuilder.expectHeader("Expires", "Sat, 01 Jan 2000 00:00:00 GMT");
        responseSpecBuilder.expectHeader("Content-Encoding", "br");
        responseSpecBuilder.expectHeader("Strict-Transport-Security", "max-age=31536000; preload; includeSubDomains");
        responseSpecBuilder.expectHeader("X-Content-Type-Options", "nosniff");
        responseSpecBuilder.expectHeader("X-XSS-Protection", "0");
        responseSpecBuilder.expectHeader("X-Frame-Options", "DENY");
        responseSpecBuilder.expectHeader("Content-Type", "text/html; charset=\"utf-8\"");
        responseSpecBuilder.expectHeader("X-FB-Debug", "u7DKWRa5WZUi/lQxR6+k2tvEfVnrKNpkw+gL9qkRCnnL34zf/iSnkioI2iqQGvkZSl2HUy6wuEwO5m8uEYKdvQ==");
        responseSpecBuilder.expectHeader("Priority", "u=3,i");
        responseSpecBuilder.expectHeader("X-FB-TRIP-ID", "1679558926");
        responseSpecBuilder.expectHeader("Date", "Fri, 19 Mar 2021 18:17:15 GMT");
        responseSpecBuilder.expectHeader("Alt-Svc", "h3-29=\":443\"; ma=3600,h3-27=\":443\"; ma=3600");
        responseSpecBuilder.expectHeader("Transfer-Encoding", "chunked");
        responseSpecBuilder.expectHeader("Connection", "keep-alive");
        responseSpecification = responseSpecBuilder.build();

        given().spec(requestSpecification).
                when().then().
                spec(responseSpecification);
    }

    @Test
    public void GETallCustomers() {
        given().
                when().
                get(baseURL + userEndPoint).
                then().statusCode(200).log().all();
    }

    @Test
    public void GETAllCustomersWithRequestSpecification() {
        given().
                spec(requestSpecification).
                when().get().then().statusCode(200).log().all();
    }

}
