package stepImpl;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import payload.User;

import static io.restassured.RestAssured.given;


public class UserSteps {
      String baseURL = "https://api.whatsapp.com/";
      String userEndPoint = "send/?phone=40786113308&text&app_absent=0";
      private RequestSpecification requestSpecification = RestAssured.given().baseUri(baseURL).basePath(userEndPoint).contentType(ContentType.JSON);


    User userPayload;



    @Given("^I want to create a new user with (.*), (.*), (.*), (.*), (.*), (.*), (.*)$")
    public void iWantToCreateANewUserWithUSER_IDACCOUNT_IDNAMESHORT_NAMEIS_BUSINESS_PERSON_ACCOUNTHAS_SECONDARY_BUSINESS_PERSONIS_MESSENGER_ONLY_USER(String USER_ID, String ACCOUNT_ID, String NAME, String SHORT_NAME, boolean IS_BUSINESS_PERSON_ACCOUNT, boolean HAS_SECONDARY_BUSINESS_PERSON, boolean IS_MESSENGER_ONLY_USER) {
        userPayload = new User();
        userPayload.setUSER_ID("22");
        userPayload.setACCOUNT_ID("22");
        userPayload.setNAME("SIKO");
        userPayload.setSHORT_NAME("NORBERT");
        userPayload.setS_BUSINESS_PERSON_ACCOUNT(false);
        userPayload.setHAS_SECONDARY_BUSINESS_PERSON(false);
        userPayload.setIS_MESSENGER_ONLY_USER(true);

    }

    @When("^I create the user resource$")
    public void iCreateTheUserResource() {
        Response postResponse = given().spec(requestSpecification).body(userPayload).post().then().log().all().extract().response();
    }

    @Then("^the user will be created in the database with the (.*)$")
    public void theUserWillBeCreatedInTheDatabaseWithTheNAME(String NAME) {
        String actualName = userPayload.getNAME();
        Assert.assertEquals("SIKO", actualName);
    }

    @And("^an id will be assigned to this new user$")
    public void anIdWillBeAssignedToThisNewUser() {
        boolean flag;
        String createUserID = userPayload.getUSER_ID();
        if(createUserID.isBlank()){
            flag=true;
        } else {
            flag=false;
            Assert.assertFalse(flag);
        }
    }

}
