Feature: User Creation For Application Under Test
  https://api.whatsapp.com/

  Scenario: User Creation
    Given I want to create a new user with <USER_ID>, <ACCOUNT_ID>, <NAME>, <SHORT_NAME>, <IS_BUSINESS_PERSON_ACCOUNT>, <HAS_SECONDARY_BUSINESS_PERSON>, <IS_MESSENGER_ONLY_USER>
    When I create the user resource
    Then the user will be created in the database with the <NAME>
    And an id will be assigned to this new user

