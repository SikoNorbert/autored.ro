use autored;

Select * from menus;
Select * from autenthification_menu;
Select * from bug_authentification_table;
Select * from consultancy_menu;

Create table if not exists pieces(carID int(11) not null auto_increment primary key, carPieces varchar(255));
drop table pieces;
Select * from pieces;

Select distinct enumerationPieces, carPieces from consultancy_menu, pieces;

Select distinct enumerationPieces from consultancy_menu;

Select distinct customerRegistration from autenthification_menu;

Select distinct * from autenthification_menu;

Select count(customerRegistration) from autenthification_menu;

Select count(*) from autenthification_menu;

Select count(*) from consultancy_menu;

Select count(*) from pieces;

Select count(*) from menus;

Select * from menus where productCorrectness = true;

Select count(*) from menus where productCorrectness = true;

Select * from menus where productCorrectness = false;

Select count(*) from menus where productCorrectness = false;

Select * from menus where menuSection ="ULEIURI MOTOR";

Select * from menus where price between 1 and 255 order by price asc;

Select * from menus where menuID between 10 and 49;

Select products from menus;

Select listOfProducts from menus where listOfProducts IS NULL;

Select products from menus where products IS NULL;

Select price from menus where products IS NULL;

Select * from menus where products = "ULEI DE MOTOR 8100 X-CLEAN+ 5W30 5L-MOTUL" and menuSection = "ULEIURI MOTOR";

Select * from menus where menuSection = "Accesorii auto interior" and listOfProducts = "COVORAȘE AUTO";

Select count(*) from menus where menuSection = "Accesorii auto interior" and listOfProducts = "COVORAȘE AUTO";

Select * from consultancy_menu where consultancyTelephone = "0742.190.240";

Select min(price) as minimumAffordPrice from menus;

Select max(price) as expensive from menus;

Select avg(price) from menus;

Select count(price) from menus;

Select sum(price) from menus;

Select menus.menuID, consultancy_menu.enumerationPieces, menus.products
From menus
Inner join consultancy_menu on menus.menuID = consultancy_menu.consultancyID; 

Select * from carMarks Right join pieces on pieces.carID = carMarks.ID;

Select * from carMarks Left join pieces on pieces.carID = carMarks.ID;

Select * from carMarks;

Select * from carMarks where carMark = "Audi" and price = 72100;

Select * from carMarks where carMark = "BMW" and fuel = "benzina";




